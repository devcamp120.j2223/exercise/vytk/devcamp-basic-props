import Info from "./components/info";

function App() {
  return (
    <div>
      <Info firstname='Vy' lastname='Kha' favNumber={21}>Child Info components</Info>
    </div>
  );
}

export default App;
