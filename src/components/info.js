import { Component } from "react";

class Info extends Component{
    render(){
        console.log(this.props)
        let {firstname,lastname,favNumber,children}=this.props;
        return(
            <div>
                My name is {lastname} {firstname} and my favourite number is {favNumber} 
                <p>{children}</p>
            </div>
        )
    }
}

export default Info;